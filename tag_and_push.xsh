about = {}
with open($(find .  -d 2  -name '__version__.py').strip()) as f:
    exec(f.read(), about)

tag_version = f"v{about['__version__']}"

git tag @(tag_version)
git push origin @(tag_version)
