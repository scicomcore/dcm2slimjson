import dcm2slimjson

def test_nullify():
    d = {'A': 1, 'B': {'A': 1}, 'C': [{'A': 1}, {'C': [{'A': 1, 'B': 2}]}]}

    dcm2slimjson.inplace_nullify(d, 'A')

    expected = {'A': None, 'B': {'A': None},
                'C': [{'A': None}, {'C': [{'A': None, 'B': 2}]}]}

    assert(d == expected)
