# What?

This is simple command-line utility to generate a JSON representation of a dicom dataset, ignoring pixel data, binary data and the Values of Interest (VOI) transform look-up-table data (tag 00283010).

It's super simple, making use of [pydicom](https://github.com/pydicom/pydicom)'s [to_dicom_dict](https://pydicom.github.io/pydicom/dev/reference/generated/pydicom.dataelem.DataElement.html?highlight=json#pydicom.dataelem.DataElement.to_json_dict) method to do the dictionary conversion, which is subsequently filtered by nullifying all `InlineBinary` attributes and `00283010` objects.

The goal is to generate a light-weight compact representation, such that common attributes can be inspected without having to parse the source dicom image. This software was generated for a specific image collection project:  **you should definitely not use this** if you want a true [DICOM JSON model](http://dicom.nema.org/dicom/2013/output/chtml/part18/sect_F.2.html) (use [`dcm2json`](https://support.dcmtk.org/docs-snapshot/dcm2json.html) for that).

# Install

Using `pipx` (install using your favourite package manager)

```
pipx install git+https://bitbucket.org/scicomcore/dcm2slimjson.git
dcm2slimjson # use app globally
```

Further, please ensure you have an up-to-date version of `pipx`, e.g. via `brew upgrade pipx`.

Alternatively, you can use [pyinstaller](https://www.pyinstaller.org/) to compile a single executable

```
pyinstaller app.py --onefile
```

Or install in a dedicated virtual environment via [poetry](https://python-poetry.org/):

```
git clone https://bitbucket.org/scicomcore/dcm2slimjson.git
cd dcm2slimjson
poetry install
poetry run dcm2slimjson
```
