from argparse import ArgumentParser
import json
import pydicom
from .__version__ import __version__


def main():

    parser = ArgumentParser(
        'dcm2slimjson',
        description='Convert a dicom, with pixel and binary data removed, to JSON'
    )

    parser.add_argument('dcm', help='Dicom file to parse')
    parser.add_argument(
        '--version',
        action='version',
        version=f'%(prog)s {__version__}')

    args = parser.parse_args()

    out = transform(args.dcm)

    json_str = json.dumps(
        out, indent=None, sort_keys=True, separators=(',', ':')
    )

    print(json_str)

def inplace_nullify(d, key):
    '''
    Recurse through a dictionary and set the value `key` to `None`
    '''
    if isinstance(d, list):
        [inplace_nullify(_, key) for _ in d]

    if isinstance(d, dict):
        for k, v in d.items():

            if (k == key):
                d[k] = None

            if isinstance(v, (dict, list)):
                inplace_nullify(v, key)


def transform(fn):

    ds = pydicom.dcmread(fn, stop_before_pixels=True)

    # Use a large value to bypass binary data handler
    out = ds.to_json_dict(bulk_data_threshold=1e20)

    # Drop binary data
    inplace_nullify(out, 'InlineBinary')

    # Remove Value of Interest (VOI) transform data
    inplace_nullify(out, '00283010')

    return out


if __name__ == '__main__':
    main()
